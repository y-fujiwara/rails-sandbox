export interface IBeetlePosition {
  top: number;
  left: number;
}
