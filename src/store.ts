import moment from 'moment';

export interface IState {
  date: Date;
  dateString: string;
  selectedUserId: number;
}

class Store {
  public state: IState = {
    date: moment().toDate(),
    dateString: moment().format('YYYYMMDD'),
    selectedUserId: -1
  };

  public setDateAction(dateEvent: Date) {
    this.state.date = dateEvent;
    this.state.dateString = moment(dateEvent).format('YYYYMMDD');
  }

  public setUserId(id: number) {
    this.state.selectedUserId = id;
  }

  public resetUserId() {
    this.state.selectedUserId = -1;
  }
}

// シングルトン
export default new Store();
