class User < ApplicationRecord
  scope :published_after, ->(_) { p 'published after' }
  scope :published, ->(_) { p 'published' }
  scope :combined_published, lambda {
    to_lambda(:published) >> to_lambda(:published_after)
  }

  def self.to_lambda(method_name)
    # 合成自体はmethodオブジェクトやlambdaで行けるけど引数が厳密にチェックされるので使い勝手が悪い
    method(method_name)
  end

  def self.merge(*methods)
    return proc {} if methods.empty?

    methods.reduce(proc {}) do |acc, method|
      acc >> to_lambda(method)
    end
  end
end
