# README

* Requirements
  * ruby v2.5.1
  * rails v5.2.2
  * node v8.14.0
  * npm v6.4.1

* Frontend Directories
  * `src/main.ts`
    * Vue.js entry point
* Frontend Production Build
  1. `npm install`
  1. `npm run build`
      * build to `public/dist`

* RUN
  * dev
    * `bundle install`
    * `foreman start`

* references
  * https://medium.com/studist-dev/goodbye-webpacker-183155a942f6